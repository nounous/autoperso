# AutoPerso

![Powered by black magic](https://forthebadge.com/images/badges/powered-by-black-magic.svg)
![Contains cat gifs](https://forthebadge.com/images/badges/contains-cat-gifs.svg)

User-isolated personal pages for mutualised Debian server.

## Initial configuration

> "*The systemd user instance is started after the first login of a user and killed after the last session of the user is closed. Sometimes it may be useful to start it right after boot, and keep the systemd user instance running after the last session closes, for instance to have some user process running without any open session. Lingering is used to that effect.*" -- https://wiki.archlinux.org/index.php/systemd/User

As we want to use systemd services for all users, activate lingering for all users,

```bash
loginctl enable-linger username
```

